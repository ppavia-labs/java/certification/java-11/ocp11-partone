package oca.tools.lambda;

import java.util.function.Predicate;

public interface PredicateFI<T> extends Predicate<T> {
	default public void doTest (T t) {
		this.test(t);
	}
}
