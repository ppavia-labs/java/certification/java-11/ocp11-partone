package oca.tools.lambda;

public interface MyFirstFunctionalInterface <T> {
	public boolean test(T a);
}
