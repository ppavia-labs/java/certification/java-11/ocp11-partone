package oca.main;

/**
 * <h1>Controlling Flow with Branching</h1>
 * 
 * @author PPAVIA
 *
 */
public class OCAControllingFlowWithBranching {

	/**
	 * <h1>Nested Loops</h1>
	 * 
	 */
	public void nestedLoops() {
	}

	/**
	 * <h1>Adding Optional Labels</h1>
	 * 
	 * <ul>
	 * <li><code>if</code> statement, <code>switch</code> statement, and
	 * <i>loops</i>, they can all have optional labels.</li>
	 * <li>A <i>label</i> is an optional pointer to the head of a statement that
	 * allows the application flow to jump to it or break from it.</li>
	 * <li>It is a single identifier proceeded by a colon (<b>:</b>).</li>
	 * <li>They follow the same rules for formatting as identifiers.</li>
	 * </ul>
	 */
	public void optionalLabels() {
	}

	/**
	 * <h1>The <code>break</code> Statement</h1>
	 * 
	 * <p>
	 * <ul>
	 * <li>The <code>break</code> statement transfers the flow of control out to the
	 * enclosing statement.</li>
	 * <li>A <code>break</code> statement with a label parameter allows us to break
	 * out of a higher-level outer loop.</li>
	 * </ul>
	 * </p>
	 */
	public void breakStatement() {
	}

	/**
	 * <h1>The <code>continue</code> Statement</h1>
	 * 
	 * <p>
	 * <ul>
	 * <li>The <code>continue</code> statement causes flow to finish the execution
	 * of the current loop.
	 * </ul>
	 * </p>
	 */
	public void continueStatement() {
	}

	/**
	 * <h1>The <code>return</code> Statement</h1>
	 */
	public void returnStatement() {
	}

	/**
	 * <h1>Unreachable Code</h1>
	 * 
	 * <p>
	 * Unreachable code does not compile !!
	 * </p>
	 */
	public void unreachableCode() {
	}

	/**
	 * <h1>Reviewing Branching</h1>
	 */
	public void reviewingBranching() {
	}
}
