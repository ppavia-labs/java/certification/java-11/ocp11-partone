package oca.main;

public class OCADecisionMakingStatementsTest {

	public void switchStatementTest() {
		var counter = 6;

		switch (counter) {
		case 1:
			System.out.println("with default : counter = " + 1);
		default:
			System.out.println("with default : default");
		case 5:
			System.out.println("with default : counter = " + 5);
		}

		switch (counter) {
		case 1:
			System.out.println("without default : counter = " + 1);
		case 5:
			System.out.println("without default : counter = " + 5);
		}

		switch (counter) {
		case 1:
			System.out.println("with good value : counter = " + 1);
		case 6:
			System.out.println("with good value : counter = " + 6);
		default:
			System.out.println("with good value : default");
		case 5:
			System.out.println("with good value : counter = " + 5);
		}

		var name = "My Name";
		final String aFinalName = "FINAL NAME";
		final String aName = "NAME";
		switch (name) {
		case "TOTO":
			System.out.println("String : " + "TOTO");
		case aFinalName:
			System.out.println("String : " + aFinalName);
		case aName:
			System.out.println("String : " + aName);
		}

		short aShort = 4;
		final int smallInt = 15;
		final int bigInt = 1_000_000;
		switch (aShort) {
		default:
		case 1:
			System.out.println("Numeric promotion : aShort = " + 1);
		case smallInt:
			System.out.println("Numeric promotion : smallInt = " + smallInt);
		case (short) bigInt:
			System.out.println("Numeric promotion : (short) bigInt = " + bigInt);
		}
	}
}
