package oca.main;

/**
 * <h1>Constructing <code>for</code> Loops</h1>
 * 
 * @author PPAVIA
 *
 */
public class OCAForLoops {

	/**
	 * <h1>The <code>for</code> Loop</h1>
	 * 
	 * <p>
	 * <code>
	 * for (initialization(optional);booleanExpression(optional);updateStatement(optional)) {<br>
	 * ...body<br>
	 * }
	 * </code>
	 * </p>
	 * 
	 * <h2>Print Elements in Reverse</h2>
	 * <h2>Working with <code>for</code> Loops</h2>
	 * <ol>
	 * <li><b>Creating Infinite Loop</b><br>
	 * <code>for(;;)</code></li>
	 * <li><b>Adding Multiple Terms to the <code>for</code> Statement</b><br>
	 * <code>
	 * int z=0;<br> 
	 * for(long x=0, y=1; z<20 && y<14; z++, y++)
	 * </code></li>
	 * <li><b>Redeclaring a Variable in the Initialization Block</b><br>
	 * <code>
	 * int x=0;<br> 
	 * for(int x=0; x<20; x++) // does not compile
	 * </code></li>
	 * <li><b>Using Incompatible Data Types in the Initialization Block</b><br>
	 * <code>
	 * for(int x=0, long y=0; x<20; x++) // does not compile
	 * </code></li>
	 * <li><b>Using Loop Variables Outside the Loop</b><br>
	 * <code>
	 * for (int x=0;x<10;x++) {<br>
	 * ... body<br>
	 * }<br>
	 * System.out.println(x); // does not compile
	 * </code></li>
	 * </ol>
	 * 
	 * <h2>Modifying Loop Variables</h2>
	 * 
	 * <p>
	 * <i>Not recommended</i>
	 * </p>
	 */
	public void forLoop() {
	}

	/**
	 * <h1>The <i>for-each</i> Loop</h1>
	 * 
	 * <p>
	 * <code>
	 * for (datatype instance : collection) {<br>
	 * ... body<br>
	 * }
	 * </code>
	 * </p>
	 * 
	 * <ul>
	 * <li>A build-in Java array.</li>
	 * <li>An object whose type implements <code>java.lang.Iterable</code></li>
	 * </ul>
	 * 
	 * <h2>Tackling the <i>for-each</i> Statement</h2>
	 * <h2>Switching Between <i>for</i> and <i>for-each</i> Loops</h2>
	 */
	public void foreachLoop() {
	}
}
