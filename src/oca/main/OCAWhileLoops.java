package oca.main;

/**
 * <h1>Writing <code>while</code> Loops</h1>
 * 
 * @author PPAVIA
 *
 */
public class OCAWhileLoops {

	/**
	 * <h1>The <code>while</code> Statement</h1>
	 * 
	 */
	public void whileStatement() {
	}

	/**
	 * <h1>The <code>do</code>/<code>while</code> Statement</h1>
	 * 
	 * <p>
	 * <code>do</code>/<code>while</code> statement must be terminated by a
	 * semicolon
	 * </p>
	 */
	public void doWhileStatement() {
	}

	/**
	 * <h1>Comparing <code>while</code> and <code>do</code>/<code>while</code>
	 * Loops</h1>
	 */
	public void comparingWhileAndDoWhileLoops() {
	}

	/**
	 * <h1>Infinite Loops</h1>
	 */
	public void infiniteLoops() {
	}

}
