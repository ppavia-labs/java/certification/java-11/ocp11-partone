package oca.main;

public class OCAOperatorsTest {

	public void operatorPrecedence() {
		int a = (byte) 200;
		System.out.println("cast overflow : a = " + a);
	}

	public void castOverflow() {
		int a = (byte) (127 + 129 + 128);
		if (a < 0 & true) {
			System.out.println("cast overflow : a < 0 " + a);
		}
		System.out.println("cast overflow : a = " + a);
	}

	public void charOperators() {
		char c = (char) -1;
		int i = 'B';
		System.out.println("c = -1 -> " + c);
		System.out.println("i = 'B' -> " + i);
	}

	public void logicalComplementAndNegationOperatorsTest() {
		double aPositiveNumber = 1.21;
		double aNegativeNumber = -aPositiveNumber;
		aPositiveNumber = -aNegativeNumber;
		System.out.println("aPositiveNumber = " + aPositiveNumber); // 1.21
		System.out.println("aNegativeNumber = " + aNegativeNumber); // -1.21
	}

	public void arithmeticOperatorsTest() {
		double aNum = 23.5674F;
		System.out.println(aNum % 2);
		long aInt = -17l;
		System.out.println(aInt % 3);
	}
}
