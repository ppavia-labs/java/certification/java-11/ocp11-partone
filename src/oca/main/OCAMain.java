package oca.main;

public class OCAMain {
	public static void main(String... args) {
		OCAOperatorsTest ocaOperatorsTest = new OCAOperatorsTest();
		ocaOperatorsTest.logicalComplementAndNegationOperatorsTest();

		ocaOperatorsTest.arithmeticOperatorsTest();

		OCADecisionMakingStatementsTest ocaDecisionMakingStatementTest = new OCADecisionMakingStatementsTest();
		ocaDecisionMakingStatementTest.switchStatementTest();
	}
}
