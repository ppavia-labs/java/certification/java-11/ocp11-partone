package oca.operators;

/**
 * <h1>Understanding Java Operators</h1>
 * 
 * @author PPAVIA
 *
 */
public class OCAJavaOperators {

	/**
	 * <h1>Types of Operators</h1>
	 * <p>
	 * Three types of operators
	 * </p>
	 * <ul>
	 * <li>unuary operator = applied to one operand</li>
	 * <li>binary operator = applied to two operands</li>
	 * <li>ternary operator = applied to three operands</li>
	 * </ul>
	 */
	public void typesOfOperators() {
	}

	/**
	 * <h1>Operator Precedence</h1>
	 * 
	 * <table>
	 * <thead>
	 * <tr>
	 * <th><b>Operator</b></th>
	 * <th><b>Symbols and exemples</b></th>
	 * </tr>
	 * </thead> <tbody>
	 * <tr>
	 * <td>
	 * <p style="margin-right:15px">
	 * Post-unuary operators
	 * </p>
	 * </td>
	 * <td>
	 * <p style="margin-right:15px">
	 * <code>expression<b>++</b>, expression<b>--</b></code>
	 * </p>
	 * </td>
	 * </tr>
	 * <tr>
	 * <td>
	 * <p style="margin-right:15px">
	 * Pre-unuary operators
	 * </p>
	 * </td>
	 * <td>
	 * <p style="margin-right:15px">
	 * <code><b>++</b>expression, <b>--</b>expression</code>
	 * </p>
	 * </td>
	 * </tr>
	 * <tr>
	 * <td>
	 * <p style="margin-right:15px">
	 * Other unuary operators
	 * </p>
	 * </td>
	 * <td>
	 * <p style="margin-right:15px">
	 * <code><b>-</b>, <b>!</b>, <b>~</b>, <b>+</b>, <b>(type)</b></code>
	 * </p>
	 * </td>
	 * </tr>
	 * <tr>
	 * <td>
	 * <p style="margin-right:15px">
	 * Multiplication, division, modulus
	 * </p>
	 * </td>
	 * <td>
	 * <p style="margin-right:15px">
	 * <code><b>*</b>, <b>/</b>, <b>%</b></code>
	 * </p>
	 * </td>
	 * </tr>
	 * <tr>
	 * <td>
	 * <p style="margin-right:15px">
	 * Addition, substraction
	 * </p>
	 * </td>
	 * <td>
	 * <p style="margin-right:15px">
	 * <code><b>+</b>, <b>-</b></code>
	 * </p>
	 * </td>
	 * </tr>
	 * <tr>
	 * <td>
	 * <p style="margin-right:15px">
	 * Shift operators
	 * </p>
	 * </td>
	 * <td>
	 * <p style="margin-right:15px">
	 * <code><b>&lt;&lt;</b>, <b>&gt;&gt;</b>, <b>&gt;&gt;&gt;</b></code>
	 * </p>
	 * </td>
	 * </tr>
	 * <tr>
	 * <td>
	 * <p style="margin-right:15px">
	 * Relation operators
	 * </p>
	 * </td>
	 * <td>
	 * <p style="margin-right:15px">
	 * <code><b>&lt;</b>, <b>&gt;</b>, <b>&lt;=</b>, <b>&gt;=</b>, <b>instanceof</b></code>
	 * </p>
	 * </td>
	 * </tr>
	 * <tr>
	 * <td>
	 * <p style="margin-right:15px">
	 * Equal to / not equal to
	 * </p>
	 * </td>
	 * <td>
	 * <p style="margin-right:15px">
	 * <code><b>==</b>, <b>!=</b></code>
	 * </p>
	 * </td>
	 * </tr>
	 * <tr>
	 * <td>
	 * <p style="margin-right:15px">
	 * Logical operators
	 * </p>
	 * </td>
	 * <td>
	 * <p style="margin-right:15px">
	 * <code><b>&</b>, <b>^</b>, <b>|</b></code>
	 * </p>
	 * </td>
	 * </tr>
	 * <tr>
	 * <td>
	 * <p style="margin-right:15px">
	 * Short-circuit logical operators
	 * </p>
	 * </td>
	 * <td>
	 * <p style="margin-right:15px">
	 * <code><b>&&</b>, <b>||</b></code>
	 * </p>
	 * </td>
	 * </tr>
	 * <tr>
	 * <td>
	 * <p style="margin-right:15px">
	 * Ternary operators
	 * </p>
	 * </td>
	 * <td>
	 * <p style="margin-right:15px">
	 * <code>boolean expression <b>?</b> expression1 <b>:</b> expression2</code>
	 * </p>
	 * </td>
	 * </tr>
	 * <tr>
	 * <td>
	 * <p style="margin-right:15px">
	 * Assignment operators
	 * </p>
	 * </td>
	 * <td>
	 * <p style="margin-right:15px">
	 * <code><b>=</b>, <b>+=</b>, <b>-=</b>, <b>*=</b>, <b>/=</b>, <b>%=</b>, <b>&=</b>, <b>^=</b>, <b>|=</b>, <b>&lt;&lt;=</b>, <b>&gt;&gt;=</b>, <b>&gt;&gt;&gt;=</b></code>
	 * </p>
	 * </td>
	 * </tr>
	 * </tbody>
	 * </table>
	 */
	public void operatorPrecedence() {
	}
}
