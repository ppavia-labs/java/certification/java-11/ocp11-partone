package oca.operators;

/**
 * <h1>Comparing Values</h1>
 * 
 * @author PPAVIA
 *
 */
public class OCAComparingValues {

	/**
	 * <h1>Equality Operators</h1>
	 * 
	 * <ul>
	 * <li><b>==</b></li>
	 * <li><b>!=</b></li>
	 * <li>Applied to primitives : return true or false if the two values represent
	 * the same value.</li>
	 * <li>Applied to object : return true or false if the two values reference the
	 * same object.</li>
	 * </ul>
	 */
	public void equalityOperators() {
	}

	/**
	 * <h1>Relational Operators</h1>
	 * 
	 * <ul>
	 * <li><b>&lt;</b></li>
	 * <li><b>&gt;</b></li>
	 * <li><b>&lt;=</b></li>
	 * <li><b>&gt;=</b></li>
	 * <li><b>instanceof</b><br>
	 * <code>null instanceof Object // return false</code></li>
	 * </ul>
	 */
	public void relationalOperators() {
	}

	/**
	 * <h1>Logical Operators</h1>
	 * 
	 * <table>
	 * <thead>
	 * <tr>
	 * <th><b>Operator</b></th>
	 * <th><b>Description</b></th>
	 * </tr>
	 * </thead> <tbody>
	 * <tr>
	 * <td>
	 * <p style="margin-right:15px">
	 * <b>&</b>
	 * </p>
	 * </td>
	 * <td>
	 * <p style="margin-right:15px">
	 * Logical AND is <code>true</code> only if both values are <code>true</code>.
	 * </p>
	 * </td>
	 * </tr>
	 * <tr>
	 * <td>
	 * <p style="margin-right:15px">
	 * <b>|</b>
	 * </p>
	 * </td>
	 * <td>
	 * <p style="margin-right:15px">
	 * Incluse OR is <code>true</code> if at least one of the values is
	 * <code>true</code>.
	 * </p>
	 * </td>
	 * </tr>
	 * <tr>
	 * <td>
	 * <p style="margin-right:15px">
	 * <b>^</b>
	 * </p>
	 * </td>
	 * <td>
	 * <p style="margin-right:15px">
	 * Exclusive XOR is <code>true</code> only if one value is <code>true</code> and
	 * the other is <code>false</code>.
	 * </p>
	 * </td>
	 * </tr>
	 * </tbody>
	 * </table>
	 */
	public void logicalOperators() {
	}

	/**
	 * <h1>Relational Operators</h1>
	 * 
	 * <ul>
	 * <li><b>&&</b></li>
	 * <li><b>||</b></li>
	 * </ul>
	 * 
	 * <h2>Avoiding a NullPointerException</h2>
	 * <h2>Checking for Unperformed Side Effect</h2>
	 */
	public void shortCircuitOperators() {
	}
}
