package oca.operators;

/**
 * <h1>Applying Unuary Operators</h1>
 * 
 * <table>
 * <thead>
 * <tr>
 * <th><b>Operator</b></th>
 * <th><b>Description</b></th>
 * </tr>
 * </thead> <tbody>
 * <tr>
 * <td>
 * <p style="margin-right:15px">
 * !
 * </p>
 * </td>
 * <td>
 * <p style="margin-right:15px">
 * Invert a boolean's logical value
 * </p>
 * </td>
 * </tr>
 * <tr>
 * <td>
 * <p style="margin-right:15px">
 * +
 * </p>
 * </td>
 * <td>
 * <p style="margin-right:15px">
 * Indicates a number is positif
 * </p>
 * </td>
 * </tr>
 * <tr>
 * <td>
 * <p style="margin-right:15px">
 * -
 * </p>
 * </td>
 * <td>
 * <p style="margin-right:15px">
 * Indicates a literate number is negative or negate an expression
 * </p>
 * </td>
 * </tr>
 * <tr>
 * <td>
 * <p style="margin-right:15px">
 * ++
 * </p>
 * </td>
 * <td>
 * <p style="margin-right:15px">
 * Increments a value by 1
 * </p>
 * </td>
 * </tr>
 * <tr>
 * <td>
 * <p style="margin-right:15px">
 * --
 * </p>
 * </td>
 * <td>
 * <p style="margin-right:15px">
 * Decrements a value by 1
 * </p>
 * </td>
 * </tr>
 * <tr>
 * <td>
 * <p style="margin-right:15px">
 * (type)
 * </p>
 * </td>
 * <td>
 * <p style="margin-right:15px">
 * Casts a value to a specific type
 * </p>
 * </td>
 * </tr>
 * </tbody>
 * </table>
 * 
 * @author PPAVIA
 *
 */
public class OCAUnuaryOperators {

	/**
	 * <h1>Logical Complement and Negation Operators</h1>
	 * 
	 * <ul>
	 * <li><i>logical complement operator</i> (<b>!</b>) flips the value of a
	 * boolean.</li>
	 * <li><i>negation operator</i> (<b>-</b>) reverses the sign of an
	 * expression.</li>
	 * </ul>
	 */
	public void logicalComplementAndNegationOperators() {
	}

	/**
	 * <h1>Increment and Decrement Operators</h1>
	 * 
	 * <ul>
	 * <li><i>pre-increment and pre-decrement operators</i> : the operator is
	 * applied first and the value is returned.</li>
	 * <li><i>post-increment and post-decrement operators</i> : the original value
	 * of the expression is returned, then operator is applied.</li>
	 * </ul>
	 */
	public void incrementAndDecrementOperators() {
	}
}
