package oca.operators;

import oca.main.OCAOperatorsTest;

/**
 * <h1>Working with Binary Arithmetic Operators</h1>
 * 
 * <ul>
 * <li><b>+</b></li>
 * <li><b>-</b></li>
 * <li><b>*</b></li>
 * <li><b>/</b></li>
 * <li><b>%</b></li>
 * </ul>
 * 
 * @author PPAVIA
 *
 */
public class OCABinaryArithmeticOperators {

	/**
	 * <h1>Arithmetic Operators</h1>
	 * 
	 * <h2>Adding Parentheses</h2>
	 * <h3>Changing the Order of Operation</h3>
	 * <h3>Verifying Parentheses Syntax</h3>
	 */
	public void arithmeticOperators() {
	}

	/**
	 * <h1>Division and Modulus Operators</h1>
	 * <ul>
	 * <li>The modulus operator is called <i>remainder operator</i></li>
	 * <li><i>remainder operator</i> can also be applied to <i>floating-point</i>
	 * number and <i>negative</i> number</li>
	 * <li><i>remainder operator</i> can return floating-point number :
	 * {@link OCAOperatorsTest#arithmeticOperatorsTest}</li>
	 * <li><i>remainder operator</i> can return negative number :
	 * {@link OCAOperatorsTest#arithmeticOperatorsTest}</li>
	 * <li><i>floor value</i> is the value without anything after the decimal
	 * point.</li>
	 * </ul>
	 */
	public void divisionAndModulusOperators() {

	}

	/**
	 * <h1>Numeric Promotion</h1>
	 * 
	 * <ul>
	 * <li>If two values have different data types, Java will automatically promote
	 * one of the values to the larger of the two data types.</li>
	 * <li>If one of the values is integral and the other is floating-point, Java
	 * will automatically promote the integral value to the floating-point value's
	 * data type.</li>
	 * <li>Smaller data types, namelly <code>byte, short, char</code> are first
	 * promoted to <code>int</code> any time they're used with a Java binary
	 * arithmetic operator, even if neither of the operands is <code>int</code></li>
	 * <li>After all promotion has occurred and the operands have the same data
	 * type, the resulting value will have the same data type as its promoted
	 * operands.</li>
	 */
	public void numericPromotion() {

	}
}
