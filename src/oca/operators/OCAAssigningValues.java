package oca.operators;

/**
 * <h1>Assigning Values</h1>
 * 
 * @author PPAVIA
 *
 */
public class OCAAssigningValues {

	/**
	 * <h1>Assignment Operator</h1>
	 * 
	 * <p>
	 * An <i>assignment operator</i> is a binary operator that modifies, or
	 * <i>assigns</i>, the variable on the left side of the operator, with the
	 * result of the value on the right side of the equation.
	 * </p>
	 */
	public void assignmentOperator() {
	}

	/**
	 * <h1>Casting Values</h1>
	 * 
	 * <p>
	 * <i>Casting</i> is a unuary operation where one data type is explicitly
	 * interpreted as another data type.
	 * </p>
	 * 
	 * <ul>
	 * <li>Casting is optional and unnecessary when converting to a larger or
	 * widening data type.</li>
	 * <li>Casting is required when converting to a smaller or narrowing data
	 * type.</li>
	 * </ul>
	 * 
	 * <h2>Overflow and Underflow</h2>
	 * 
	 * <ul>
	 * <li><i>Overflow</i> is when a number is too large for the data type field so
	 * the system <i>wraps around</i> to the lowest negative value and counts up
	 * from there.</li>
	 * <li><i>Underflow</i> is when a number is too low such as stortin -200 in a
	 * <code>byte</code> field.</li>
	 * </ul>
	 */
	public void castingValues() {
	}

	/**
	 * <h1>Compound Assignment Operators</h1>
	 * 
	 * <ul>
	 * <li><b>+=</b></li>
	 * <li><b>-=</b></li>
	 * <li><b>*=</b></li>
	 * <li><b>/=</b></li>
	 * </ul>
	 */
	public void compoundAssignmentOperators() {
	}

	/**
	 * <h1>Assignment Operator Return Value</h1>
	 * 
	 * <p>
	 * <code>long coyote = (wolf=3);</code><br>
	 * First wolf value
	 * </p>
	 */
	public void assignmentOperatorReturnValue() {
	}
}
