package oca.welcometojava;

/**
 * <h1>Ordering Elements in a Class</h1>
 * <table>
 * <thead>
 * <tr>
 * <th><b>Element</b></th>
 * <th><b>Example</b></th>
 * <th><b>Required</b></th>
 * <th><b>Where does it go?</b></th>
 * </tr>
 * </thead> <tbody style="text-align:left">
 * <tr>
 * <td>
 * <p style="margin-right:15px">
 * Package declaration
 * </p>
 * </td>
 * <td>
 * <p style="margin-right:15px">
 * <code>package abc;</code>
 * </p>
 * </td>
 * <td>
 * <p style="margin-right:15px">
 * No
 * </p>
 * </td>
 * <td>
 * <p style="margin-right:15px">
 * First line in the file
 * </p>
 * </td>
 * </tr>
 * <tr>
 * <td>
 * <p style="margin-right:15px">
 * Import statements
 * </p>
 * </td>
 * <td>
 * <p style="margin-right:15px">
 * <code>import java.util.*;</code>
 * </p>
 * </td>
 * <td>
 * <p style="margin-right:15px">
 * No
 * </p>
 * </td>
 * <td>
 * <p style="margin-right:15px">
 * Immediatly after the package (if present)
 * </p>
 * </td>
 * </tr>
 * <tr>
 * <td>
 * <p style="margin-right:15px">
 * Class declaration
 * </p>
 * </td>
 * <td>
 * <p style="margin-right:15px">
 * <code>public class ClassA</code>
 * </p>
 * </td>
 * <td>
 * <p style="margin-right:15px">
 * Yes
 * </p>
 * </td>
 * <td>
 * <p style="margin-right:15px">
 * Immediatly after the import (if any)
 * </p>
 * </td>
 * </tr>
 * <tr>
 * <td>
 * <p style="margin-right:15px">
 * Field declarations
 * </p>
 * </td>
 * <td>
 * <p style="margin-right:15px">
 * <code>int values;</code>
 * </p>
 * </td>
 * <td>
 * <p style="margin-right:15px">
 * No
 * </p>
 * </td>
 * <td>
 * <p style="margin-right:15px">
 * Any top-level element in a class
 * </p>
 * </td>
 * </tr>
 * <tr>
 * <td>
 * <p style="margin-right:15px">
 * Method declarations
 * </p>
 * </td>
 * <td>
 * <p style="margin-right:15px">
 * <code>void aMethod()</code>
 * </p>
 * </td>
 * <td>
 * <p style="margin-right:15px">
 * No
 * </p>
 * </td>
 * <td>
 * <p style="margin-right:15px">
 * Any top-level element in a class
 * </p>
 * </td>
 * </tr>
 * </tbody>
 * </table>
 * 
 * @author PPAVIA
 *
 */
public class OCAOrderingElementsInClass {

}
