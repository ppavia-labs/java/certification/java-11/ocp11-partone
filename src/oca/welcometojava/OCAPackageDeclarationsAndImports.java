package oca.welcometojava;

/**
 * <h1>Understanding Package Declarations and Imports</h1>
 * 
 * @author PPAVIA
 *
 */
public class OCAPackageDeclarationsAndImports {

	/**
	 * <h1>Wildcards</h1>
	 * <p>
	 * Import all the classes in the same package (ONLY classes, not packages,
	 * fields or methods).<br/>
	 * Java compiler imports just what's actually needed.
	 * </p>
	 */
	public void wildcards() {
	}

	/**
	 * <h1>Redundand Imports</h1>
	 * <ul>
	 * <li>A wildcar only matches class names.</li>
	 * <li>We can only have one wildcard and it must be at the end.</li>
	 * <li>We cannot import methods only class names.</li>
	 * </ul>
	 */
	public void redundandImports() {
	}

	/**
	 * <h1>Naming Conflicts</h1>
	 * <p>
	 * When the class is found in multiple packages, Java gives us a compile error :
	 * <br/>
	 * <code>error : reference to 'Class' is ambiguous</code>
	 * </p>
	 * <ul>
	 * <li>Explicit import take precedence over any wildcards present.</li>
	 * <li>If we need two classes with the same name, we have to use fully qualified
	 * name for one of them.</li>
	 * </ul>
	 */
	public void namingConflicts() {
	}

	/**
	 * <h1>Creating a New Package</h1>
	 * <p>
	 * Java packages are related to directory structure on the computer.
	 * </p>
	 */
	public void creatingNewPackage() {
	}

	/**
	 * <h1>Compiling and Running Code with Packages</h1>
	 * <h2>uses of javac command</h2>
	 * <ul>
	 * <li>Use space to separate different classe names to compile.</li>
	 * <li>Wildcard can be used to specify all java classes in the same
	 * package.</li>
	 * <li>Wildcard cannot be used to include subdirectories.</li>
	 * </ul>
	 */
	public void compilingAndRunningCodeWithPackages() {
	}

	/**
	 * <h1>Using an Alternate Directory</h1>
	 * <p>
	 * By default, the javac command places the compiled classes in the same
	 * directory as the source code.<br/>
	 * The option <b>-d</b> place the compiled classes into specified directory and
	 * <b>preserve</b> the package structure.
	 * </p>
	 * <h2>javac command options</h2>
	 * <ul>
	 * <li><b>-d</b> : Specify compiled classes directory</li>
	 * <li>Classpath options : Location of classes needed to compile the program.
	 * <ul>
	 * <li><code>javac -cp classes packageA/ClassA.java</code></li>
	 * <li><code>javac -classpath classes packageA/ClassA.java</code></li>
	 * <li><code>javac --class-path classes packageA/ClassA.java</code></li>
	 * </ul>
	 * </li>
	 * <li><b><code>-m, --module</b> [module-name]</code> : Compile only the
	 * specified module, check timestamps</li>
	 * </ul>
	 * 
	 * <h2>java command options</h2>
	 * <ul>
	 * <li>Classpath options : Location of classes needed to run the program.
	 * <ul>
	 * <li><code>java -cp classes packageA.ClassA</code></li>
	 * <li><code>java -classpath classes packageA.ClassA</code></li>
	 * <li><code>java --class-path classes packageA.ClassA</code></li>
	 * </ul>
	 * </li>
	 * <li><b><code>-p, --module-path</b> [module path]</code> : A <b>;</b> separate
	 * directory.</li>
	 * </ul>
	 */
	public void usingAlternateDirectory() {
	}

	/**
	 * <h1>Compiling with JAR Files</h1>
	 * <p>
	 * <h2>On windows system</h2>
	 * <code>java -cp ".;C:\tmp\someOtherLocation;\tmp\myJar.jar" packageA.ClassA</code>
	 * <h2>On linux system</h2>
	 * <code>java -cp ".:/tmp/someOtherLocation:/tmp/myJar.jar" packageA.ClassA</code>
	 * </p>
	 */
	public void compilingWithJARFiles() {
	}

	/**
	 * <h1>Creating a JAR File</h1>
	 * <h2>Command to create a jar file</h2>
	 * <ul>
	 * <li>Create a jar file containing the files in the current directory :
	 * <ul>
	 * <li><code>jar -cvf myJar.jar</code></li>
	 * <li><code>jar --create --verbose --file myJar.jar</code></li></li>
	 * </ul>
	 * <li><b>-c, --create</b> : Creates a new JAR file.</li>
	 * <li><b>-v, --verbose</b> : Prints details when working with JAR files.</li>
	 * <li><b>-f, --file</b> [file name] : JAR filename.</li>
	 * <li><b>-C</b> [directory]: Directory containing files to be used to create
	 * the JAR.</li>
	 * </ul>
	 */
	public void creatingJARFile() {
	}

	/**
	 * <h1>Running Program in One Line with Packages</h1>
	 * <p>
	 * We can use single-file source-code programs from within a package as long as
	 * they rely only on classes supplied by the JDK.
	 * </p>
	 * <ul>
	 * <li><code>java ClassA.java</code></li>
	 * <li><code>java packageA/ClassA.java</code></li>
	 * </ul>
	 */
	public void runningProgramInOneLineWithPackages() {
	}
}
