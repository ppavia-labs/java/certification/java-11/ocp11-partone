package oca.welcometojava;

/**
 * <h1>Understanding the Java Class Structure</h1>
 * 
 * @author PPAVIA
 *
 */
public class OCAJavaClassStructure {

	/**
	 * <h1>Fields and Methods</h1>
	 * <h2>Caracteristics of methods</h2>
	 * <ul>
	 * <li>Method name</li>
	 * <li>Return type</li>
	 * <li>Parameters of the method</li>
	 * <li>Method signature :
	 * <ul>
	 * <li>Method name</li>
	 * <li>Parameters of the method</li>
	 * </ul>
	 * </li>
	 * <li>Method declaration :
	 * <ul>
	 * <li>access modifier</li>
	 * <li>optional specifier</li>
	 * <li>return type</li>
	 * <li>method name</li>
	 * <li>parantheses</li>
	 * <li>list of parameters</li>
	 * <li>exception</li>
	 * </ul>
	 * </li>
	 * </ul>
	 */
	public void fieldsAndMethods() {

	}

	/**
	 * <h1>Comments</h1>
	 * 
	 * <p>
	 * Les trois types de commentaires java :
	 * </p>
	 * <ul>
	 * <li><code>// single line comment</code></li>
	 * <li><code>/* Mutliple-line comment </code></li>
	 * <li><code>/** Javadoc multiple-line comment</code></li>
	 * </ul>
	 */

	public void comments() {
		// single line comment

		/*
		 * Mutliple-line comment
		 */

		/**
		 * Javadoc multiple-line comment
		 */
	}

	/**
	 * <h1>Class Vs File</h1>
	 * 
	 * <h2>Rules for a writting a class</h2>
	 * <ul>
	 * <li>Each file can contain only one public class</li>
	 * <li>The class does not have to be public (default access modifier works
	 * too)</li>
	 * <li>The file name must match the class name and have a .java extension</li>
	 * <li>More than one class could be declared in the same file</li>
	 * <li>If more than one class are declared in the same file, just one must be
	 * declared public</li>
	 * </ul>
	 * 
	 */
	public void classVsFile() {
	}
}
