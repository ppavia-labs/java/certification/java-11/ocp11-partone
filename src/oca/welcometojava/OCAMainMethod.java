package oca.welcometojava;

public class OCAMainMethod {

	/**
	 * <h1>Creating a main() Method</h1>
	 * <h2>Rules for writting a class</h2>
	 * <ul>
	 * <li>Each file can contain only one public class</li>
	 * <li>The class does not have to be public</li>
	 * <li>The file name must match the class name and have a .java extension</li>
	 * </ul>
	 */
	public void creatingMainMethod() {
	}

	/**
	 * <h1>Passing Parameters to Java Program</h1>
	 * <code>java ClassName param1 param2 "a param with specials chars"</code>
	 */
	public void passingParametersToJavaProgram() {
	}

	/**
	 * <h1>Running Program in One Line</h1>
	 * <p>
	 * Since Java 11 , we can run a program without compiling it first. The code is
	 * being compiled in memory.
	 * </p>
	 * <h2>Single-file source-code</h2>
	 * <ul>
	 * <li>The <code>java</code> command passes the name of the Java file
	 * *.java.</li>
	 * <li>The program can be used only if it is one file.</li>
	 * <li>The file can contain only one class in it.</li>
	 * <li>The file can only import code that came with the JDK.</li>
	 * <li>If .class exists when executing program in one line an
	 * <code>error: class found
	 * on application class path:</code>is throwned</li>
	 * </ul>
	 */
	public void runningProgramInOneLine() {
	}
}
