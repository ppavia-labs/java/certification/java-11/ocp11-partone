package oca.welcometojava;

/**
 * <h1>Benefits Of Java</h1>
 * <ul>
 * <li>Oriented Object</li>
 * <li>Encapsulation</li>
 * <li>Platform Independent</li>
 * <li>Robust</li>
 * <li>Simple</li>
 * <li>Secure</li>
 * <li>MultiThreading</li>
 * <li>Backward Compatibility</li>
 * </ul>
 * 
 * @author PPAVIA
 *
 */
public class OCABenefitsOfJava {

}
