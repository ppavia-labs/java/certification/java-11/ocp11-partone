package oca.javabuildingblocks;

/**
 * <h1>Managing Variable Scope</h1>
 * 
 * @author PPAVIA
 *
 */
public class OCAVariableScope {

	/**
	 * <h1>Limiting Scope</h1>
	 * 
	 * <p>
	 * Local variables can never have a scope larger than the method they are
	 * defined in.
	 * </p>
	 */
	public void limitingScope() {
	}

	/**
	 * <h1>Nesting Scope</h1>
	 * 
	 * <p>
	 * The smaller container blocks can reference variables defined in the larger
	 * scoped blocks, but not vie versa.
	 * </p>
	 */
	public void nestingScope() {
	}

	/**
	 * <h1>Tracing Scope</h1>
	 */
	public void tracingScope() {
	}

	/**
	 * <h1>Applying Scope to Classes</h1>
	 * 
	 * <ul>
	 * <li><i>instance variables</i> are available as soon as they are defined and
	 * last for the entire lifetime of the object itself.</li>
	 * <li><i>class variable</i> stay in scope for the entire life of the
	 * program.</li>
	 * </ul>
	 */
	public void applyingScopeToClasses() {
	}
}
