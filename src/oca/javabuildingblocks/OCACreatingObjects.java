package oca.javabuildingblocks;

/**
 * <h1>Creating Objects</h1>
 * 
 * @author PPAVIA
 *
 */
public class OCACreatingObjects {

	/**
	 * <h1>Calling Constructors</h1>
	 * <ul>
	 * <li>The name of the constructor must match the name of the class</li>
	 * <li>No return type</li>
	 * </ul>
	 */
	public void callingConstructors() {
	}

	/**
	 * <h1>Reading and Writing Member Fields</h1>
	 * <ul>
	 * <li>Instance variables</li>
	 * <li>Set variables</li>
	 * <li>Getters and Setters</li>
	 * </ul>
	 */
	public void readingAndWritingMemberFields() {
	}

	/**
	 * <h1>Executing Instance Initializer Blocks</h1>
	 * <ul>
	 * <li><code>{...}</code> : code block.</li>
	 * <li>Instance initializers = code block declared outside a method.</li>
	 * <li>Getters and Setters</li>
	 * </ul>
	 */
	public void executingInstanceInitializerBlocks() {
	}

	/**
	 * <h1>Following Order of Initialization</h1>
	 * <ul>
	 * <li>Fields and instance initializer blocks are run in the order in which they
	 * appear in the file.</li>
	 * <li>The constructor runs after all fields and instance initializer blocks
	 * have run.</li>
	 * </ul>
	 * 
	 * <h2>Sequence of execution</h2>
	 * <ol>
	 * <li>main() method.</li>
	 * <li>Fields and instance initializer blocks are run in the order in which they
	 * appear in the file.</li>
	 * <li>The constructor runs after all fields and instance initializer blocks
	 * have run.</li>
	 * </ol>
	 */
	public void followingOrderOfInitialization() {
	}
}
