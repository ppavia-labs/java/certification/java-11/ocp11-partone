package oca.javabuildingblocks;

/**
 * <h1>Destroying Object</h1>
 * 
 * <p>
 * Garbage collector automatically look for objects that aren't needed anymore.
 * </p>
 * <ul>
 * <li>The object no longer has any references pointing on it.</li>
 * <li>All references to the object have gone out of scope.</li>
 * <li>garbage collector call <code>finalize()</code> once. If GC failed to
 * collect the object, there was no second call to <code>finalize()</code></li>
 * </ul>
 * 
 * @author PPAVIA
 *
 */
public class OCADestroyingObject {

}
