package oca.javabuildingblocks;

/**
 * <h1>Declaring Variables</h1>
 * 
 * @author PPAVIA
 *
 */
public class OCADeclaringVariables {

	/**
	 * <h1>Identifying Identifiers</h1>
	 * 
	 * <ul>
	 * <li>Identifiers must begin with a letter, & or _.</li>
	 * <li>Identifiers can include numbers but not start with them.</li>
	 * <li>Since Java 9, a single underscore '_' is not allowed as an
	 * identifier.</li>
	 * <li>We can not use Java reserved word.</li>
	 * </ul>
	 * 
	 * <p>
	 * camelCase and snake_case!
	 * </p>
	 */
	public void identifyingIdentifiers() {
	}

	/**
	 * <h1>Declaring Multiple Variables</h1>
	 * <ul>
	 * <li>We can declare many variables of the same type in the same
	 * statement.</li>
	 * <li>Declaration of many varialbles in the same statement must share the same
	 * type declaration.</li>
	 * </ul>
	 */
	public void declaringMultipleVariables() {
		int i = 5, j = 6, z; // be carefull with z, it is not initialized.
	}

}
