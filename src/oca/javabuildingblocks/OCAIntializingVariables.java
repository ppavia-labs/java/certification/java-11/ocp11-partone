package oca.javabuildingblocks;

/**
 * <h1>Intializing Variables</h1>
 * 
 * @author PPAVIA
 *
 */
public class OCAIntializingVariables {

	/**
	 * <h1>Creating Local Variables</h1>
	 * <p>
	 * A <i>local variable</i> is a variable defined in a constructor, method or
	 * initializer block.
	 * </p>
	 * 
	 * <ul>
	 * <li><i>local variable</i> does not have default value.</li>
	 * <li><i>local variable</i> must be initialize before we can use it.</li>
	 * </ul>
	 */
	public void creatingLocalVariables() {
	}

	/**
	 * <h1>Passing Constructor and Method Parameters</h1>
	 * <ul>
	 * <li>Variables passed to a constructor or a method are called <i>constructor
	 * parameters</i> or <i>method parameters</i>.</li>
	 * <li>They are <i>local variable</i>.</li>
	 * </ul>
	 */
	public void passingConstructorAndMethodParameters() {
	}

	/**
	 * <h1>Defining Instance and Class Variables</h1>
	 * 
	 * <ul>
	 * <li>An <i>instance variable</i> often called a field.</li>
	 * <li>A <i>class variable</i> is defined on the class level and shared among
	 * all instances of the class.</li>
	 * <li><i>instance variable</i> and <i>class variable</i> do not require to be
	 * initialized.</li>
	 * </ul>
	 * 
	 * <table>
	 * <thead>
	 * <tr>
	 * <th><b>Variable type</b></th>
	 * <th><b>Default initialization value</b></th>
	 * </tr>
	 * </thead> <tbody>
	 * <tr>
	 * <td>
	 * <p style="margin-right:15px">
	 * boolean
	 * </p>
	 * </td>
	 * <td>
	 * <p style="margin-right:15px">
	 * <code>false</code>
	 * </p>
	 * </td>
	 * </tr>
	 * <tr>
	 * <td>
	 * <p style="margin-right:15px">
	 * byte, short, int, long
	 * </p>
	 * </td>
	 * <td>
	 * <p style="margin-right:15px">
	 * <code>0</code>
	 * </p>
	 * </td>
	 * </tr>
	 * <tr>
	 * <td>
	 * <p style="margin-right:15px">
	 * float, double
	 * </p>
	 * </td>
	 * <td>
	 * <p style="margin-right:15px">
	 * <code>0.0</code>
	 * </p>
	 * </td>
	 * </tr>
	 * <tr>
	 * <td>
	 * <p style="margin-right:15px">
	 * char
	 * </p>
	 * </td>
	 * <td>
	 * <p style="margin-right:15px">
	 * <code>\u0000</code> (NUL)
	 * </p>
	 * </td>
	 * </tr>
	 * <tr>
	 * <td>
	 * <p style="margin-right:15px">
	 * All object references
	 * </p>
	 * </td>
	 * <td>
	 * <p style="margin-right:15px">
	 * <code>null</code>
	 * </p>
	 * </td>
	 * </tr>
	 * </tbody>
	 * </table>
	 */
	public void definingInstanceAndClassVariables() {
	}

	/**
	 * <h1>Keyword <code>var</code></h1>
	 * 
	 * <h2>Introducing <i>var</i></h2>
	 * <ul>
	 * <li><code>var</code> is used as a <i>local variable</i> in a constructor,
	 * method, or initializer block.</li>
	 * <li><code>var</code> cannot be used in constructor parameters, method
	 * parameters, instance variables, or class variables.</li>
	 * <li><code>var</code> is always initialized on the same line or statement
	 * where it is declared.</li>
	 * <li>The value of a <code>var</code> can change, but the type cannot.</li>
	 * <li><code>var</code> cannot be initialized with a <code>null</code> value
	 * without a type :<br>
	 * <code>var x = null; // does not compile</code><br>
	 * <code>var x = (String) null;</code></li>
	 * <li><code>var</code> is not permitted in a multiple-variable declaration
	 * :<br>
	 * <code>int i, var x; // does not compile</code></li>
	 * <li><code>var</code> is a reserved type name but not a reserved word : it can
	 * be used as an identifier except as a class, interface, or enum name.</li>
	 * </ul>
	 */
	public void var() {
	}
}
