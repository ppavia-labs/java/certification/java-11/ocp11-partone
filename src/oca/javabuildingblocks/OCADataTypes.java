package oca.javabuildingblocks;

/**
 * <h1>Understanding Data Types</h1>
 * 
 * @author PPAVIA
 *
 */
public class OCADataTypes {

	/**
	 * <h1>Using Primitive Types</h1>
	 * <p>
	 * 8 built-in data types, referred to as Java <i>primitive types</i>. Primitives
	 * are just a single value in memory.
	 * </p>
	 * <table>
	 * <thead>
	 * <tr>
	 * <th><b>Keyword</b></th>
	 * <th><b>Type</b></th>
	 * <th><b>Example</b></th>
	 * </tr>
	 * </thead> <tbody>
	 * <tr>
	 * <td>
	 * <p style="margin-right:15px">
	 * boolean
	 * </p>
	 * </td>
	 * <td>
	 * <p style="margin-right:15px">
	 * true or false
	 * </p>
	 * </td>
	 * <td>
	 * <p style="margin-right:15px">
	 * <code>true</code>
	 * </p>
	 * </td>
	 * </tr>
	 * <tr>
	 * <td>
	 * <p style="margin-right:15px">
	 * byte
	 * </p>
	 * </td>
	 * <td>
	 * <p style="margin-right:15px">
	 * 8-bit integral value
	 * </p>
	 * </td>
	 * <td>
	 * <p style="margin-right:15px">
	 * <code>123</code>
	 * </p>
	 * </td>
	 * </tr>
	 * <tr>
	 * <td>
	 * <p style="margin-right:15px">
	 * short
	 * </p>
	 * </td>
	 * <td>
	 * <p style="margin-right:15px">
	 * 16-bit integral value
	 * </p>
	 * </td>
	 * <td>
	 * <p style="margin-right:15px">
	 * <code>123</code>
	 * </p>
	 * </td>
	 * </tr>
	 * <tr>
	 * <td>
	 * <p style="margin-right:15px">
	 * int
	 * </p>
	 * </td>
	 * <td>
	 * <p style="margin-right:15px">
	 * 32-bit integral value
	 * </p>
	 * </td>
	 * <td>
	 * <p style="margin-right:15px">
	 * <code>123</code>
	 * </p>
	 * </td>
	 * </tr>
	 * <tr>
	 * <td>
	 * <p style="margin-right:15px">
	 * long
	 * </p>
	 * </td>
	 * <td>
	 * <p style="margin-right:15px">
	 * 64-bit integral value
	 * </p>
	 * </td>
	 * <td>
	 * <p style="margin-right:15px">
	 * <code>123L</code>
	 * </p>
	 * </td>
	 * </tr>
	 * <tr>
	 * <td>
	 * <p style="margin-right:15px">
	 * float
	 * </p>
	 * </td>
	 * <td>
	 * <p style="margin-right:15px">
	 * 32-bit floating-point value
	 * </p>
	 * </td>
	 * <td>
	 * <p style="margin-right:15px">
	 * <code>123.45f</code>
	 * </p>
	 * </td>
	 * </tr>
	 * <tr>
	 * <td>
	 * <p style="margin-right:15px">
	 * double
	 * </p>
	 * </td>
	 * <td>
	 * <p style="margin-right:15px">
	 * 64-bit floating-point value
	 * </p>
	 * </td>
	 * <td>
	 * <p style="margin-right:15px">
	 * <code>123.456</code>
	 * </p>
	 * </td>
	 * </tr>
	 * <tr>
	 * <td>
	 * <p style="margin-right:15px">
	 * char
	 * </p>
	 * </td>
	 * <td>
	 * <p style="margin-right:15px">
	 * 16-bit Unicode value (unsigned)
	 * </p>
	 * </td>
	 * <td>
	 * <p style="margin-right:15px">
	 * <code>'A'</code>
	 * </p>
	 * </td>
	 * </tr>
	 * </tbody>
	 * </table>
	 * 
	 * <h2>The Primitive Types</h2>
	 * <ul>
	 * <li>The <code>float</code> and <code>double</code> are used for
	 * floating-point (decimal) value.</li>
	 * <li>A <code>float</code> requires the letter <b>f</b> or <b>F</b> following
	 * the number.</li>
	 * <li><code>byte</code>, <code>short</code>, <code>int</code>,
	 * <code>long</code> types are used for numbers without decimal points. In
	 * mathematics, these are all referred to as integral value.</li>
	 * <li>All of the numeric types are signed in Java : One of their bits is
	 * reserved to cover a negative range</li>
	 * <li>integers can store char value : <br>
	 * <code>short d = 'd'; // print 100</code><br>
	 * <code>char c = (short)83; // print 'S'</code><br>
	 * <code>char c = -1; // does not compile</code><br>
	 * <code>char c = (char)-1; // print ''</code><br>
	 * <code>int i = 'B'; // print 66</code><br>
	 * </li>
	 * </ul>
	 * 
	 * <h2>Writing Literals</h2>
	 * <ul>
	 * <li><i>long</i> : <code>long aLong = 0l;</code> or
	 * <code>long aLong = 0L;</code></li>
	 * <li><i>float</i> : <code>float aFloat = 0.0f;</code> or
	 * <code>float aFloat = 0.0F;</code></li>
	 * <li>Octal (digit 0-7) : <code>int octal = 017;</code></li>
	 * <li>Hexadecimal (digit 0-9, letter A-F) :
	 * <code>int octal = 0xFF004B;</code></li>
	 * <li>Binary (digit 0-1) : <code>int octal = 0b101100;</code></li>
	 * </ul>
	 * 
	 * <h2>Literals and the Underscore Character</h2>
	 * <p>
	 * Underscore character is used to make the number easier to read.
	 * </p>
	 * <ul>
	 * <li>Not at the start : <code>int i = _1000; // does not compile</code></li>
	 * <li>Not at the end : <code>int i = 1000_; // does not compile</code></li>
	 * <li>Not by decimal :
	 * <code>double d = 1000_.0; // does not compile</code></li>
	 * <li>Correct but annoying : <code>int i = 1_00_0_0; // compiles</code></li>
	 * <li>Correct but annoying : <code>int i = 1_________0; // compiles</code></li>
	 * </ul>
	 */
	public void usingPrimitiveTypes() {
	}

	/**
	 * <h1>Using Reference Types</h1>
	 * <ul>
	 * <li>A reference can be assigned to another object of the same or compatible
	 * type.</li>
	 * <li>A reference can be assigned to a new object using the <code>new</code>
	 * keyword</li>
	 * </ul>
	 */
	public void UsingReferenceTypes() {
	}

	/**
	 * <h1>Distinguishing between Primitives and Reference Types</h1>
	 * <ul>
	 * <li>A Primitive can not be assigned <code>null</code>.</li>
	 * <li>Primitives do not have methods declared on them.</li>
	 * </ul>
	 */
	public void PrimitivesVsReferenceTypes() {
	}

}
