package oca.makingdecisions;

/**
 * <h1>Creating Decision-Making Statements</h1>
 * 
 * @author PPAVIA
 *
 */
public class OCADecisionMakingStatements {

	/**
	 * <h1>Statements and Blocks</h1>
	 */
	public void statementsAndBlocks() {
	}

	/**
	 * <h1>The <i>if</i> Statement</h1>
	 */
	public void ifStatement() {
	}

	/**
	 * <h1>The <i>else</i> Statement</h1>
	 */
	public void elseStatement() {
	}

	/**
	 * <h1>The <i>switch</i> Statement</h1>
	 * 
	 * <h2>Proper Switch Syntax</h2>
	 * <p>
	 * <code>
	 * int month = 5;<br>
	 * swtich(month) {<br>
	 * case 1 :<br>
	 * break;<br>
	 * case 5 :<br>
	 * break;<br>
	 * default:<br>
	 * } 
	 * </code>
	 * </p>
	 * 
	 * <h2>Switch Data Types</h2>
	 * <p>
	 * <code>switch</code> statement has a target variable that is not evaluated
	 * until runtime.
	 * </p>
	 * <ul>
	 * <li><code>byte</code> and <code>Byte</code></li>
	 * <li><code>short</code> and <code>Short</code></li>
	 * <li><code>char</code> and <code>Character</code></li>
	 * <li><code>int</code> and <code>Integer</code></li>
	 * <li><code>String</code></li>
	 * <li><code>enum</code> value</li>
	 * <li><code>var</code> (if the type resolves to one of the preceding
	 * types)</li>
	 * </ul>
	 * 
	 * <h2>Switch Control Flow</h2>
	 * 
	 * <ul>
	 * <li>If there is no <code>break</code> statement, Java jumps to the
	 * <code>default</code> block if there is no good case value or to the good case
	 * value, if they exist.<br>
	 * Then, Java will execute all proceeding case statements in order.</li>
	 * <li>If a <code>break</code> statement exists in a good case value, Java stops
	 * flow execution.</li>
	 * <li>The <code>case</code> value must be known at <b>compile-time</b>, so it
	 * must be marked <code>final</code> !</li>
	 * <li><b>Numeric Promotion and Casting</b> : <code>switch</code> statements
	 * support numeric promotion that does not require an explicit cast.</li>
	 * </ul>
	 */
	public void switchStatement() {
	}
}
