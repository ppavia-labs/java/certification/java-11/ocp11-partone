package ocp.javatime;

import java.time.Instant;
import java.time.ZonedDateTime;

public class OCPZonedDateTime {

	public void instant() {
		ZonedDateTime zdt = ZonedDateTime.now();
		Instant i = zdt.toInstant();
		System.out.println(zdt);
		System.out.println(i);

		i = ZonedDateTime.now().toInstant();
		System.out.println(i);
	}
}
