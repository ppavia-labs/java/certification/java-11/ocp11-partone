package ocp.flashcards.gc;

public class OCPFlashCardGc {

	public static void main(String[] args) {
		Thread thread = new Thread(new OCAaThread());
		thread.start();
		OCPToGc ocpToGc = new OCPToGc();
		ocpToGc = null;
		System.gc();
	}

}
