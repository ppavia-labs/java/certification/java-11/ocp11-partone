package ocp.annotations;

public class OCPIntoducingAnnotations {

    /**
     * <h1>Understanding Metadata</h1>
     */
    public void metadata() {
    }

    /**
     * <h1>Purpose of Annotations</h1>
     *
     * <p>Assign metadata attributes to classes, methods, variables or other Java types.</p>
     */
    public void purposeOfAnnotations() {
    }
}
