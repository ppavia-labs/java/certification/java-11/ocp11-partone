package ocp.main;

import oca.main.OCAOperatorsTest;

public class OCPMain {
	public static void main(String... args) {
		OCAOperatorsTest ocaOperatorsTest = new OCAOperatorsTest();
		ocaOperatorsTest.logicalComplementAndNegationOperatorsTest();
	}
}
