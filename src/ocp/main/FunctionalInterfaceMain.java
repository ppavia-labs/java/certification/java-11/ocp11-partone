package ocp.main;

import oca.tools.beans.Animal;
import oca.tools.lambda.MyFirstFunctionalInterface;

public class FunctionalInterfaceMain {
	private static void print(Animal a, MyFirstFunctionalInterface<Animal> i) {
		if(i.test(a)) {
			System.out.println(a);
		}
		else {
			System.out.println("test failed");
		}
	}
	
	public static void main (String... args) {
		print(new Animal("fish", false, true), a -> a.canHop());
		print(new Animal("dog", true, true), a -> a.canHop());
	}
	
	public static <T extends Animal> T canHop(T t) {
		return t;
	}
}
